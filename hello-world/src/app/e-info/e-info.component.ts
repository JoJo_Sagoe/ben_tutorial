import { Component, OnInit, Type } from '@angular/core';
import { DataService } from '../data.service';

@Component({
  selector: 'app-e-info',
  templateUrl: './e-info.component.html',
  styleUrls: ['./e-info.component.css'],
  providers: [DataService]
})
export class EInfoComponent implements OnInit {

  infoReceived1: string[]= [];
  infoReceived2: string[]= [];
  infoReceived3: string[]= [];
  infoReceived4: string[]= [];
  infoReceived5: string[]= [];

  //create methods to retreive info for each
  getInfoFromService1() {
    this.infoReceived1 = this.dservice.getInfo1()
  }

  getInfoFromService2() {
    this.infoReceived2 = this.dservice.getInfo2()
  }

  getInfoFromService3() {
    this.infoReceived3 = this.dservice.getInfo3()
  }

  getInfoFromService4() {
    this.infoReceived4 = this.dservice.getInfo4()
  }

  getInfoFromService5() {
    this.infoReceived5 = this.dservice.getInfo5()
  } //from here create the records in the service file

  constructor(private dservice: DataService) { } //create a property to access the instance of data service created
  //this property can help to retreive data from the service in the getInfoFromService methods above

  ngOnInit(): void {
  }

}
