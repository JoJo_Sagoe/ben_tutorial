import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  //create the records for the employees here
  info1: string[] = ["John Wall", "EID101", "jw@es.com"]
  info2: string[] = ["Diamond Whyte", "EID164", "dw@es.com"]
  info3: string[] = ["Jayda Parker", "EID005", "jp@es.com"]
  info4: string[] = ["Kasper Knowells", "EID250", "kk@es.com"]
  info5: string[] = ["Jojo Kwaintsir Sagoe", "EID001", "jks@es.com"]

  //create method to return the employee records
  getInfo1(): string[] {  //getInfo method, type is string of type array and is returning the data for the first record
    return this.info1
  }

  getInfo2(): string[] {
    return this.info2
  }

  getInfo3(): string[] {
    return this.info3
  }

  getInfo4(): string[] {
    return this.info4
  }

  getInfo5(): string[] {
    return this.info5
  }  // when done, and import dependency injection on the e-info component file and include in providers array

  constructor() { }
}
